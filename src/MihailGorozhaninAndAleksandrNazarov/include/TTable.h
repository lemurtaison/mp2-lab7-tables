#pragma once

#include "TDatacom.h"
#include "TTabRecord.h"

typedef TTabRecord* PTTabRecord;

class  TTable : public TDataCom {
protected:
    int DataCount;                                                   // ���������� ������� � �������
    int Efficiency;                                                  // ���������� ������������� ���������� ��������
public:
    TTable() { DataCount = 0; Efficiency = 0; }                      // �����������
    virtual ~TTable() {};                                            // ����������
                                                                     // �������������� ������
    int GetDataCount() const { return DataCount; }                   // �-�� �������
    int GetEfficiency() const { return Efficiency; }                 // �������������
    int IsEmpty() const { return DataCount == 0; }                   // �����?
    virtual bool IsFull() const = 0;                                  // ���������?
                                                                     // ������
    virtual TKey GetKey(void) const = 0;
		virtual PTDatValue GetValuePtr(void) const = 0;
    // �������� ������
    virtual PTDatValue FindRecord(TKey k) = 0;                      // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal) = 0;            // ��������
    virtual void DelRecord(TKey k) = 0;                             // ������� ������
                                                                    // ���������
    virtual void Reset(void) = 0;                                    // ���������� �� ������ ������
    virtual bool IsTabEnded(void) const = 0;                         // ������� ���������?
    virtual int GoNext(void) = 0;                                   // ������� � ��������� ������
                                                                    // (=1 ����� ���������� ��� ��������� ������ �������)
};