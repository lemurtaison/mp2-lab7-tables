﻿#pragma once

#include "TTable.h"

class  THashTable : public TTable {
protected:
    unsigned long HashFunc(const TKey& key);
public:
    static const int TAB_MAX_SIZE = 25;
    THashTable() : TTable() {}
};