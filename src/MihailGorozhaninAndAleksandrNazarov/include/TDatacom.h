#pragma once


#define TabOK	0 // ������ ���
// ���� ��������
#define TabNoRec 100 // ��� ������
// ���� ������
#define TabError -102 // ������ � �������
#define TabNoMem -101 // ��� ������


class TDataCom
{
protected:
	int RetCode;

	int SetRetCode(int ret) { return RetCode = ret; }
public:
	TDataCom() : RetCode(TabOK) {}
	virtual ~TDataCom() = 0 {}
	int GetRetCode()
	{
		int temp = RetCode;
		RetCode = TabOK;
		return temp;
	}
};

