#pragma once

#include "../include/TScanTable.h"
#include "../include/TSortTable.h"
#include "../include/TArrayHash.h"
#include "../include/TTreeTable.h"

#include <iostream>
#include <fstream>
#include <string>
#include <conio.h>
#include <vector>

using namespace std;

class TTableViewer {
public:
    TTableViewer() { };
    void PushMessage(string message);

private:

    enum UsingTable { Scan, Sort, Hash, Tree };

    UsingTable Use = Scan;
    TTable* Table = new TScanTable();
    vector<string> arg;
    string FilePath = "";

    static int StrToInt(string str);
    vector<string> MessageToArgs(string message);

    void Help();
    void Delete(string str);
    void Find(string str);
    void Insert(string str);
    void Replace(string str);

    double AverageMarks();
    string BestStudent();
    double AverageMarkOfSubject(int NumOfSub);
    void InformationAboutStudent(string student);
    void HighAchievers();

    void Save(string str);
    void Load(string str);

    void Show();
};