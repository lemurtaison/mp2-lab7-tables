#pragma once

#include <iostream>
#include <string>
#include "TDatValue.h"

using namespace std;

typedef string TKey;                                                                    // ��� ����� ������
                                                                                        // ����� ��������-�������� ��� ������� �������
    class TTabRecord : public TDatValue {
    protected:                                                                          // ����    
        TKey Key;                                                                       // ���� ������
        PTDatValue pValue;                                                              // ��������� �� ��������
    public:                                                                             // ������
        TTabRecord(TKey k = "", PTDatValue pVal = NULL): Key(k), pValue(pVal) { }       // ����������� 
        void SetKey(TKey k) { Key = k; }                                                // ���������� �������� �����
        TKey GetKey(void) { return Key; }                                               // �������� �������� �����
        void SetValuePtr(PTDatValue p) { pValue = p; }                                  // ���������� ��������� �� ������
				PTDatValue GetValuePtr(void) const { return pValue; }                            // �������� ��������� �� ������
				virtual TDatValue * GetCopy() {                                                 // ���������� �����
            TDatValue * temp = new TTabRecord(Key, pValue);
            return temp;
        }                                                 
        TTabRecord & operator = (TTabRecord &tr) {                                     // ������������
            Key = tr.Key; pValue = tr.pValue;
            return *this;
        }
        virtual int operator == (const TTabRecord &tr) { return Key == tr.Key; }       // ��������� =
        virtual int operator < (const TTabRecord &tr) { return Key < tr.Key; }         // ��������� �<�
        virtual int operator > (const TTabRecord &tr) { return Key > tr.Key; }         // ��������� �>�

                                                                                        // ������������� ������ ��� ��������� ����� ������, ��. �����
        friend class TArrayTable;
        friend class TScanTable;
        friend class TSortTable;
        friend class TTreeNode;
        friend class TTreeTable;
        friend class TArrayHash;
        friend class TListHash;
};