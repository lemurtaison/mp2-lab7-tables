#pragma once

#include "TTreeTable.h"
#include "TBalanceNode.h"

enum class Height { OK, Inc };

class  TBalanceTree : public TTreeTable {
protected:
	Height InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	Height LeftTreeBalancing(PTBalanceNode &pNode);  // ������. ������ ���������
	Height RightTreeBalancing(PTBalanceNode &pNode); // ������. ������� ���������
public:
	TBalanceTree() :TTreeTable() {} // �����������
																	//�������� ������
	virtual void InsRecord(TKey k, PTDatValue pVal) override final; // ��������
	virtual void DelRecord(TKey k) override final;                  // �������
};

typedef TBalanceTree *PTBalanceTree;
