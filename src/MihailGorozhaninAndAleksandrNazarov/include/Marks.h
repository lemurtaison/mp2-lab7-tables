#pragma once

#include "TDatValue.h"

#define Subjects 5
#define AlgorithmsAndStructureOfData 0
#define OptimizationMetods 1
#define OperationSystems 2
#define DevelopingPO 3
#define DevelopingInstruments 4

class Marks : public TDatValue {
 public:

    int Values[Subjects];
    
    TDatValue* GetCopy() {
        return new Marks(Values);
    }

    Marks(int Values[Subjects]) {
        for (int i = 0; i < Subjects; i++)
            this->Values[i] = Values[i];
    }

    Marks() {
        for (int i = 0; i < Subjects; i++)
            Values[i] = 0;
    }

    ~Marks() { }
};