#pragma once

#include "TTreeNode.h"

enum class Bal { BalOK, BalLeft, BalRight };

class  TBalanceNode : public TTreeNode {
protected:
	Bal Balance; // индекс балансировки вершины
public:
	TBalanceNode(TKey k = "", PTDatValue pVal = NULL, PTTreeNode pL = NULL,
		PTTreeNode pR = NULL, Bal bal = Bal::BalOK) : TTreeNode(k, pVal, pL, pR),
		Balance(bal) {};			// конструктор
	virtual TDatValue * GetCopy();  // изготовить копию
	Bal GetBalance(void) const;
	void SetBalance(Bal bal);
	friend class TBalanceTree;
};

typedef TBalanceNode *PTBalanceNode;
