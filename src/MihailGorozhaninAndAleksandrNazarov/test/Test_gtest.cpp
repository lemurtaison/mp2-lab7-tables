#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <gtest/gtest.h>
#include "TArrayTable.h"
#include "../src/TArrayTable.cpp"
#include "TScanTable.h"
#include "TSortTable.h"
#include "../src/TScanTable.cpp"
#include "../src/TSortTable.cpp"
#include <gtest/gtest.h>
#include <TTreeNode.h>
#include <../src/TTreeNode.cpp>
#include <TBalanceNode.h>
#include <../src/TBalanceNode.cpp>
#include "TTreeTable.h"
#include "../src/TTreeTable.cpp"
#include "TBalanceTree.h"
#include "../src/TBalanceTree.cpp"
#include "Marks.h"

TEST(ScanTable, Can_Create_ScanTable_Number) {
	ASSERT_NO_THROW(TScanTable table(5));
}

TEST(ScanTable, Can_Insert_Record) {
	TScanTable* table = new TScanTable(5);
	int i = 123;

	ASSERT_NO_THROW((table->InsRecord("abc", (TDatValue*)&i)));
}

TEST(ScanTable, Can_Get_Table_Size) {
	TScanTable* table = new TScanTable(5);

	EXPECT_EQ(5, table->GetTabSize());
}

TEST(TScanTable, can_create_table) {
	ASSERT_NO_THROW(TScanTable testing_table);
}

TEST(TScanTable, cant_create_table_with_incorrect_size) {
	ASSERT_ANY_THROW(TScanTable testing_table(-1));
}

TEST(TScanTable, can_insert_record) {
	TScanTable testing_table;
	int Test_Marks[Subjects];
	for (int i = 0; i < Subjects; i++)
		Test_Marks[i] = i;
	TDatValue* Data = new Marks(Test_Marks);
	Marks* Mark;
	testing_table.InsRecord("test_key", Data);
	Mark = (Marks*)testing_table.FindRecord("test_key");
	EXPECT_TRUE(Mark->Values[2] == 2);
}

TEST(ScanTable, Full_Table_Is_Full) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}

	ASSERT_TRUE(table->IsFull());
}

TEST(ScanTable, Can_Set_Current_Position) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->SetCurrentPos(2);

	ASSERT_TRUE("aaac" == table->GetKey());
}

TEST(ScanTable, Can_Get_Current_Position) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->SetCurrentPos(3);

	EXPECT_EQ(3, table->GetCurrentPos());
}

TEST(ScanTable, Can_Reset_Position) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->SetCurrentPos(3);
	table->Reset();

	EXPECT_EQ(0, table->GetCurrentPos());
}

TEST(ScanTable, Can_Go_Next_Record) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->GoNext();
	table->GoNext();

	bool correctness = (2 == table->GetCurrentPos()) &&
		("aaac" == table->GetKey());
	ASSERT_TRUE(correctness);
}

TEST(ScanTable, Can_Find_Record) {
	TScanTable* table = new TScanTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->InsRecord("aaaa", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("aaab", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("aaba", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("aabc", (TDatValue*)i);

	EXPECT_EQ(2, *((int*)(table->FindRecord("aaab"))));
}

TEST(ScanTable, Can_Delete_Record) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}

	ASSERT_NO_THROW(table->DelRecord("aaac"));
	EXPECT_TRUE(nullptr == table->FindRecord("aaac"));
}

TEST(SortTable, Can_Use_Different_Sort_Methods) {
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaaa", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaab", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 3;
	table->SetSortMethod(QUICK_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaac", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaad", (TDatValue*)&i[0]));

	EXPECT_EQ(3, *((int*)(table->FindRecord("aaac"))));
}
TEST(TabRecord, Can_Create_TabRecord) {
	ASSERT_NO_THROW(TTabRecord rec1("abc"));
}

TEST(TabRecord, Can_Set_Key) {
	TTabRecord rec1;
	ASSERT_NO_THROW(rec1.SetKey("abc"));
}

TEST(TabRecord, Can_Get_Key) {
	string s = "cba";
	TTabRecord rec1("abc");

	rec1.SetKey(s);

	bool keys_equality = (s == rec1.GetKey()) && (rec1.GetKey() != "abc");

	ASSERT_TRUE(keys_equality);
}

TEST(TabRecord, Can_Set_Value) {
	TTabRecord rec1("abc");
	int val = 123;

	ASSERT_NO_THROW(rec1.SetValuePtr((PTDatValue)&val));
}

TEST(TabRecord, Can_Get_Value) {
	TTabRecord rec1("abc");
	int val = 123, expected_val;

	rec1.SetValuePtr((PTDatValue)&val);

	expected_val = *(int*)(rec1.GetValuePtr());

	EXPECT_EQ(val, expected_val);
}

TEST(TabRecord, Records_With_Equal_Keys_Are_Equal) {
	TTabRecord rec1("abc");
	TTabRecord rec2("abc");

	ASSERT_TRUE(rec1 == rec2);
}

TEST(TabRecord, Records_With_Not_Equal_Keys_Are_Not_Equal) {
	TTabRecord rec1("abc");
	TTabRecord rec2("cba");

	ASSERT_FALSE(rec1 == rec2);
}

TEST(TabRecord, Record_With_Bigger_Key_Is_Less) {
	TTabRecord rec1("aaa"), rec2("aab");

	ASSERT_TRUE(rec2 > rec1);
}

TEST(TabRecord, Record_With_Less_Key_Is_Less) {
	TTabRecord rec1("aaa"), rec2("aab");

	ASSERT_TRUE(rec1 < rec2);
}

TEST(TabRecord, Can_Assign_Record) {
	TTabRecord rec1("abc"), rec2;

	rec2 = rec1;

	ASSERT_TRUE(rec1 == rec2);
}

TEST(TabRecord, Assigned_Record_Has_Its_own_Mem) {
	TTabRecord rec1("abc"), rec2;

	rec2 = rec1;
	rec2.SetKey("cba");

	ASSERT_FALSE(rec1 == rec2);
}
TEST(TreeNode, Can_Create_Tree_Node) {
	ASSERT_NO_THROW(TTreeNode node());
}

TEST(TreeNode, Can_Place_Nodes_In_Tree) {
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("aaaa", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("aaac", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("aaab", (PTDatValue)&i[0], &left, &right);

	EXPECT_EQ(Center.GetLeft(), &left);
	EXPECT_EQ(Center.GetRight(), &right);
}

TEST(BalanceNode, Can_Create_Balanced_Tree_Node) {
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TreeTable, Can_Create_Tree_Table) {
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TreeTable, Can_Insert_Record) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	ASSERT_NO_THROW(table->InsRecord("aaaa", (PTDatValue)&i[0]));
}

TEST(TreeTable, Can_Get_Key) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("aaaa", (PTDatValue)&i[0]);
	table->Reset();
	EXPECT_EQ("aaaa", table->GetKey());
}

TEST(TreeTable, Can_Get_Value) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("aaaa", (PTDatValue)&i[0]);
	table->Reset();
	EXPECT_EQ(3, *((int*)table->GetValuePtr()));
}

TEST(TreeTable, Can_Find_Record) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("aaaa", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("aaae", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("aaad", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("aaae"), 5);
}

TEST(BalanceTree, Can_Create_Balanced_Tree) {
	ASSERT_NO_THROW(TBalanceTree tree);
}

TEST(BalanceTree, Can_Add_And_Find_Record) {
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("aaai", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("aaba", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("acaa", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("aaab", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("aage", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("aaab"), 2);
}


	

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
