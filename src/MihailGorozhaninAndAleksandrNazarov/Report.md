# Методы программирования **2**
***
#Лабораторная работа №**7**: Таблицы
***
##Введение:
***
>Представление данных во многих задачах из разных областей человеческой деятельности может быть организовано при помощи таблиц. Таблицы представляют собой последовательности строк (записей), структура строк может быть различной, но обязательным является поле, задающее имя (ключ) записи. Таблицы применяются в бухгалтерском учете (ведомости заработной платы), в торговле (прайс-листы), в образовательных учреждениях (экзаменационные ведомости) и являются одними из наиболее распространенных структур данных, используемых при создании системного и прикладного математического обеспечения. Таблицы широко применяются в трансляторах (таблицы идентификаторов) и операционных системах, могут рассматриваться как программная реализация ассоциативной памяти и т.п. Существование отношения «иметь имя» является обязательным в большинстве разрабатываемых программистами структур данных; доступ по имени в этих структурах служит для получения соответствия между адресным принципом указания элементов памяти ЭВМ и общепринятым (более удобным для человека) способом указания объектов по их именам.

***
##Основные понятия и определения
***
>**Таблица** (от лат. tabula – доска) – динамическая структура данных, базисным множеством которой является семейство линейных структур из записей (базисное отношение включения определяется операциями вставки и удаления записей).

>**Запись** – кортеж, каждый элемент которого обычно именуется полем.

>**Имя записи (ключ)** – одно из полей записи, по которому обычно осуществляется поиск записей в таблице; остальные поля образуют тело записи.
>**Двоичное дерево поиска** – это представление данных в виде дерева, для которого выполняются условия:
>
>- для любого узла (вершины) дерева существует не более двух потомков (двоичное дерево);
>- для любого узла значения во всех узлах левого поддерева меньше значения в узле;
>- для любого узла значения во всех узлах правого поддерева больше значения в узле.

>**Хеш-функция** – функция, ставящая в соответствие ключу номер записи в таблице (используется при организации таблиц с вычислимым входом).

***
##Требования к лабораторной работе
***
В рамках данной лабораторной работы ставится задача создания программных средств, поддерживающих табличные динамические структуры данных (таблицы) и базовые операции над ними:

- поиск записи;
- вставка записи (без дублирования);
- удаление записи.

Выполнение операций над таблицами может осуществляться с различной степенью эффективности в зависимости от способа организации таблицы.

В рамках лабораторной работы как показатель эффективности предлагается использовать количество операций, необходимых для выполнения операции поиска записи в таблице. Величина этого показателя должна определяться как аналитически (при использовании тех или иных упрощающих предположений), так и экспериментально на основе проведения вычислительных экспериментов.

>В лабораторной работе предлагается реализовать следующие типы таблиц:
>
- просмотровые (неупорядоченные);
- упорядоченные (сортированные);
- таблицы со структурами хранения на основе деревьев поиска;
- хеш-таблицы или перемешанные (с вычисляемыми адресами).

Необходимо разработать интерфейс доступа к операциям поиска, вставки и удаления, не зависящий от способа организации таблицы.

Демонстрацию работоспособности разрабатываемых при выполнении лабораторной работы программ следует проводить на примере таблиц, содержащих данные о результатах экзаменационной успеваемости студентов. Для данного примера следует реализовать следующие операции:

1) для таблицы с результатами успеваемости отдельной студенческой группы:

- получение сведений об успеваемости отдельного студента (оценка по конкретному предмету, средняя оценка по всем предметам);
- определение средней оценки по группе по отдельному предмету или по всем предметам;
- подсчет количества студентов-отличников и т.п.

2) для нескольких таблиц с результатами успеваемости для всех студенческих групп курса:

- получение средних оценок по всем студенческим группам по отдельному предмету или по всем предметам;
- определение студенческой группы с лучшей успеваемостью по конкретному предмету (или по всем предметам экзаменационной сессии);
- подсчет количества студентов-отличников для всего курса и т.п.

Задача обработки результатов успеваемости может быть расширена возможностью хранения данных для нескольких экзаменационных сессий (например, за весь период обучения студентов).

Указанный выше набор операций обработки результатов успеваемости является минимально необходимым; определение полного перечня необходимых операций должно выполняться на этапе постановки лабораторной работе (в том числе, например, могут быть выбраны различающиеся наборы операций для получения разных постановок лабораторной работы).

Результаты обработки успеваемости рекомендуется дополнять построением графиков и диаграмм различного вида (например, диаграммы по средним баллам для разных студенческих групп, графики успеваемости студентов по разным предметам).

***
##Условия и ограничения
***
Сделаем следующие основные допущения:

>1. В качестве ключа будем рассматривать фамилию и имя, в качестве полей данных – экзаменационные оценки по учебным дисциплинам.

>2. Сведения об экзаменационной успеваемости должны быть разнесены по разным таблицам для каждой студенческой группы в отдельности.

>3. Исходные данные – количество и наименование дисциплин, данные о результатах сессии – должны извлекаться из текстовых файлов (для каждой студенческой группы в отдельности).

>4. При изменении состояния таблиц должна обеспечиваться возможность сохранения данных в текстовых файлах.

>5. Для контроля правильности работы программ должна обеспечиваться возможность пакетного выполнения операций (наименование операций и их параметры задаются при помощи текстового файла). После выполнения пакета операций и сохранения данных должна быть реализована возможность сравнения полученных файлов с заранее подготовленными проверочными файлами.

***
##Методы выполнения лабораторной работы
***

###Выполнение работы

Как и ранее, выполнение лабораторной работы должно быть разделено на несколько этапов. Каждый из этапов должен иметь достаточно небольшую длительность, при этом должна обеспечиваться возможность решения поставленной прикладной задачи (в той или иной ограниченной постановке) на как можно более ранних этапах выполнения работ.

С учетом высказанных рекомендаций последовательность разработки программ может быть следующей:

**Этап 1.** Реализация программ для работы с просматриваемыми таблицами (классы TTabRecord, TTable, TArrayTable, TScanTable). Выполнение этапа может быть разделено на несколько итераций:

>- Реализация некоторой упрощенной схемы начального заполнения таблицы;
>- Реализация операции поиска;
>- Реализация операции вставки;
>- Реализация операции удаления.

Результаты каждой итерации должны быть протестированы – переход к следующей итерации разработки должен осуществляться только после успешного выполнения всех запланированных тестов.

**Этап 2.** Реализация первой очереди операций прикладной задачи из постановки лабораторной работы (статистическая обработка результатов экзаменационной сессии). В числе реализуемых операций может быть, например, ввод исходных данных из текстовых файлов, получение данных об успеваемости отдельных студентов, вычисление средних оценок по отдельным предметам и др.

**Этап 3.** Реализация программ для работы с упорядоченными таблицами (класс TSortTable). Для проверки правильности работы программ результаты выполнения должны сравниваться с результатами, получаемыми при помощи просматриваемых (и надежно проверенных) таблиц.

**Этап 4.** Реализация программ для работы с таблицами на основе деревьев поиска (классы TTreeNode, TTreeTable).

**Этап 5.** Реализация программ для работы с таблицами с вычислимым входом (классы THashTable, TArrayHash).

**Этап 6.** Выполнение вычислительных экспериментов для оценки эффективности различных способов организации таблиц. Разработка различных генераторов последовательности выполняемых операций (генерация только операций поиска, генерация потока операций всех типов, генерация потоков операций с равномерным использованием ключей и т.п.).

***
##Разработка программного комплекса
***

###Структура проекта

При выполнении данной лабораторной работы следует разработать иерархию классов, учитывая, что все таблицы имеют как общие свойства (их описание следует поместить в определении базового класса), так и особенности выполнения отдельных операций (реализуются в отдельных классах для каждого вида таблиц). 

При разработке классов используется ранее разработанный класс TDatValue.
Рекомендуемый состав классов приведен ниже.

>- TTabRecord.h, TTabRecord.cpp – модуль с классом объектов-значений для записей таблицы;
- TTable.h – абстрактный базовый класс, содержит спецификации методов таблицы;
- TArrayTable.h, TArrayTable.cpp – абстрактный базовый класс для таблиц с непрерывной памятью;
- TScanTable.h, TScanTable.cpp – модуль с классом, обеспечивающим реализацию просматриваемых таблиц;
- TSortTable.h, TSortTable.cpp – модуль с классом, обеспечивающим реализацию упорядоченных таблиц;
- TTreeNode.h, TTreeNode.cpp – модуль с абстрактным базовым классом объектов-значений для деревьев;
- TTreeTable.h, TTreeTable.cpp – модуль с классом, реализующим таблицы в виде деревьев поиска;
- TBalanceNode.h, TBalanceNode.cpp – модуль с базовым классом объектов-значений для сбалансированных деревьев;
- TBalanceTree.h, TBalanceTree.cpp – модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска;
- THashTable.h, THashTable.cpp – модуль с базовым классом, обеспечивающим реализацию таблиц с вычислимым входом;
- TArrayHash.h, TArrayHash.cpp – модуль с классом, обеспечивающим реализацию хеш-таблиц (разрешение коллизий на основе открытого перемешивания);
- TTableTestkit.cpp – модуль программы тестирования.


### Цели работы:   


##### В рамках лабораторной работы ставятся следующие цели: 

- изучение способов организации таблиц
- начальное знакомство с принципами проектирования структур хранения, используемых в методах решения прикладных задач.
- освоение и улучшение реализации крупных задач в команде (парная работа)
 
***


### Задачи:  
1. Разработка интерфейса всех необходимых классов перечисленных в пункте **Структура проекта**. (Доработка уже готовых и разработка пустых)
2. Реализация методов всех необходимых классов согласно заданному интерфейсу.(см. пункт **Структура проекта**)
3. Разработка программы в которой будут продемонстрированы возможные способы организации таблиц, при рассмотрении проблемы статистической обработки результатов экзаменационной успеваемости студентов (выполнение таких, например, вычислений как определение среднего балла по предмету и/или по группе при назначении студентов на стипендию или при распределении студентов по кафедрам и т.п.).
4. Разработка тестов для проверки работоспособности редактора таблиц.
5. Реализация алгоритма работы с таблицами.
6. Реализация операций для работы с таблицами.
7. Обеспечение работоспособности тестов и примера использования.

***

### Используемые инструменты:

***
- Система контроля версий Git. 
- Фреймворк для написания автоматических тестов Google Test.
- Среда разработки Microsoft Visual Studio (2017).
***


## Начало работы:

***
### 1. Разработка и реализация класса `TDatacom`:
***

#### Объявление и реализация (h-файл):
```
#pragma once


#define TabOK	0 // ошибок нет
// коды ситуаций
#define TabNoRec 100 // Нет записи
// коды ошибок
#define TabError -102 // ошибка в таблице
#define TabNoMem -101 // нет памяти


class TDataCom
{
protected:
	int RetCode;

	int SetRetCode(int ret) { return RetCode = ret; }
public:
	TDataCom() : RetCode(TabOK) {}
	virtual ~TDataCom() = 0 {}
	int GetRetCode()
	{
		int temp = RetCode;
		RetCode = TabOK;
		return temp;
	}
};
```
***
### 2. Разработка и реализация абстрактного класса объектов-значений `TDatValue`:
***
#### Объявление и реализация (h-файл):
```
#pragma once

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
```

***
### 3. Разработка и реализация базового класса для звеньев (элементов) списка `Marks` :
***
#### Объявление (h-файл):
```
#pragma once

#include "TDatValue.h"

#define Subjects 5
#define AlgorithmsAndStructureOfData 0
#define OptimizationMetods 1
#define OperationSystems 2
#define DevelopingPO 3
#define DevelopingInstruments 4

class Marks : public TDatValue {
 public:

    int Values[Subjects];
    
    TDatValue* GetCopy() {
        return new Marks(Values);
    }

    Marks(int Values[Subjects]) {
        for (int i = 0; i < Subjects; i++)
            this->Values[i] = Values[i];
    }

    Marks() {
        for (int i = 0; i < Subjects; i++)
            Values[i] = 0;
    }

    ~Marks() { }
};
```
***
### 4. Разработка и реализация класса `TTabRecord`:
***

#### Объявление (h-файл):
```
#pragma once

#include <iostream>
#include <string>
#include "TDatValue.h"

using namespace std;

typedef string TKey;                                                                    // тип ключа записи
                                                                                        // Класс объектов-значений для записей таблицы
    class TTabRecord : public TDatValue {
    protected:                                                                          // поля    
        TKey Key;                                                                       // ключ записи
        PTDatValue pValue;                                                              // указатель на значение
    public:                                                                             // методы
        TTabRecord(TKey k = "", PTDatValue pVal = NULL): Key(k), pValue(pVal) { }       // конструктор 
        void SetKey(TKey k) { Key = k; }                                                // установить значение ключа
        TKey GetKey(void) { return Key; }                                               // получить значение ключа
        void SetValuePtr(PTDatValue p) { pValue = p; }                                  // установить указатель на данные
				PTDatValue GetValuePtr(void) const { return pValue; }                            // получить указатель на данные
				virtual TDatValue * GetCopy() {                                                 // изготовить копию
            TDatValue * temp = new TTabRecord(Key, pValue);
            return temp;
        }                                                 
        TTabRecord & operator = (TTabRecord &tr) {                                     // присваивание
            Key = tr.Key; pValue = tr.pValue;
            return *this;
        }
        virtual int operator == (const TTabRecord &tr) { return Key == tr.Key; }       // сравнение =
        virtual int operator < (const TTabRecord &tr) { return Key < tr.Key; }         // сравнение «<»
        virtual int operator > (const TTabRecord &tr) { return Key > tr.Key; }         // сравнение «>»

                                                                                        // дружественные классы для различных типов таблиц, см. далее
        friend class TArrayTable;
        friend class TScanTable;
        friend class TSortTable;
        friend class TTreeNode;
        friend class TTreeTable;
        friend class TArrayHash;
        friend class TListHash;
};
```

***
### 5. Разработка и реализация класса  `TTable`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TDatacom.h"
#include "TTabRecord.h"

typedef TTabRecord* PTTabRecord;

class  TTable : public TDataCom {
protected:
    int DataCount;                                                   // количество записей в таблице
    int Efficiency;                                                  // показатель эффективности выполнения операции
public:
    TTable() { DataCount = 0; Efficiency = 0; }                      // конструктор
    virtual ~TTable() {};                                            // деструктор
                                                                     // информационные методы
    int GetDataCount() const { return DataCount; }                   // к-во записей
    int GetEfficiency() const { return Efficiency; }                 // эффективность
    int IsEmpty() const { return DataCount == 0; }                   // пуста?
    virtual bool IsFull() const = 0;                                  // заполнена?
                                                                     // доступ
    virtual TKey GetKey(void) const = 0;
		virtual PTDatValue GetValuePtr(void) const = 0;
    // основные методы
    virtual PTDatValue FindRecord(TKey k) = 0;                      // найти запись
    virtual void InsRecord(TKey k, PTDatValue pVal) = 0;            // вставить
    virtual void DelRecord(TKey k) = 0;                             // удалить запись
                                                                    // навигация
    virtual void Reset(void) = 0;                                    // установить на первую запись
    virtual bool IsTabEnded(void) const = 0;                         // таблица завершена?
    virtual int GoNext(void) = 0;                                   // переход к следующей записи
                                                                    // (=1 после применения для последней записи таблицы)
};
```

***
### 6. Разработка и реализация класса  `TArrayTable`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TTable.h"

#define TabMaxSize 25

enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class  TArrayTable : public TTable {
protected:
    PTTabRecord *pRecs;                                                  // память для записей таблицы
    int TabSize;                                                        // макс. возм.количество записей в таблице
    int CurrPos;                                                        // номер текущей записи (нумерация с 0)
public:
    TArrayTable(int Size = TabMaxSize) {                                // конструктор
        pRecs = new PTTabRecord[Size];
        for (int i = 0; i < Size; i++) pRecs[i] = NULL;
        TabSize = Size; DataCount = CurrPos = 0;
    }
    ~TArrayTable() {                                                   // деструктор
        for (int i = 0; i < DataCount; i++)
            delete pRecs[i];
        delete[] pRecs;
    }
                                                                        // информационные методы
    virtual bool IsFull() const { return DataCount >= TabSize; }         // заполнена?
    
    int GetTabSize() const { return TabSize; }                          // к-во записей
                                                                        // доступ
    virtual TKey GetKey(void) const { return GetKey(CURRENT_POS); }
		virtual PTDatValue GetValuePtr() const override { return GetValuePTR(CURRENT_POS); }
    virtual TKey GetKey(TDataPos mode) const;
    virtual PTDatValue GetValuePTR(TDataPos mode) const;
    // основные методы
    virtual PTDatValue FindRecord(TKey k) = 0;                          // найти запись
    virtual void InsRecord(TKey k, PTDatValue pVal) = 0;                // вставить
    virtual void DelRecord(TKey k) = 0;                                 // удалить запись
                                                                        //навигация
    virtual void Reset(void);                                            // установить на первую запись
    virtual bool IsTabEnded(void) const;                                 // таблица завершена?
    virtual int GoNext(void);                                           // переход к следующей записи
                                                                        //(=1 после применения для последней записи таблицы)
    virtual int SetCurrentPos(int pos);                                 // установить текущую запись
    int GetCurrentPos(void) const { return CurrPos; }                   //получить номер текущей записи
    friend TSortTable;
};
```
#### Реализация (cpp-файл):
```
#include "../include/TArrayTable.h"

TKey TArrayTable::GetKey(TDataPos mode) const {
    int pos = -1;
    if (!IsEmpty()) {
        switch (mode) {
        case FIRST_POS: pos = 0; break;
        case LAST_POS: pos = DataCount - 1; break;
        default: pos = CurrPos; break;
        }
    }
    return (pos == -1) ? string("") : pRecs[pos]->Key;
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) const {
    int pos = -1;
    if (!IsEmpty()) {
        switch (mode) {
        case FIRST_POS: pos = 0; break;
        case LAST_POS: pos = DataCount - 1; break;
        default: pos = CurrPos; break;
        }
    }
    return (pos == -1) ? NULL : pRecs[pos]->pValue;
}

void TArrayTable::Reset(void) {
    CurrPos = 0;
}

bool TArrayTable::IsTabEnded(void)const {
    return CurrPos >= DataCount;
}

int TArrayTable::GoNext(void) {
    if (!IsTabEnded()) CurrPos++;
    return IsTabEnded();
}

int TArrayTable::SetCurrentPos(int pos) {
    CurrPos = ((pos > -1) && (pos < DataCount)) ? pos : 0;
    return IsTabEnded();
}
```

***
### 7. Разработка и реализация класса  `TScanTable`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TArrayTable.h"

class  TScanTable : public TArrayTable {
public:
    TScanTable(int Size = TabMaxSize) : TArrayTable(Size) {};   //конструктор
                                                                // основные методы
    virtual PTDatValue FindRecord(TKey k);                      //найти запись
    virtual void InsRecord(TKey k, PTDatValue pVal);            //вставить
    virtual void DelRecord(TKey k);                             //удалить запись

};
```
#### Реализация (cpp-файл):
```
#include "..\include\TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k) {
    int i;
    SetRetCode(TabOK);
    for (i = 0; i < DataCount && !(pRecs[i]->Key == k); i++);
    Efficiency = i + 1;
    if (i < DataCount) {
        CurrPos = i;
        return pRecs[i]->pValue;
    }
    SetRetCode(TabNoRec);
    return NULL;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal) {
    if (IsFull()) {
        SetRetCode(TabNoMem);
        throw "Not enough memory";
    }
    else {
        pRecs[DataCount++] = new TTabRecord(k, pVal);
    }
}

void TScanTable::DelRecord(TKey k) {
    for (int i = 0; i < DataCount; i++) {
        if (pRecs[i]->GetKey() == k) {
            for (int j = i; pRecs[j + 1] != 0 && j < TabSize; j++)
                pRecs[j] = pRecs[j + 1];
            DataCount--;
            return;
        }
    }
}
```

***
### 8. Разработка и реализация класса  `TSortTable`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TScanTable.h"

enum TSortMethod { INSERT_SORT, MERGE_SORT, QUICK_SORT };
class  TSortTable : public TScanTable {
protected:
    TSortMethod SortMethod;                                 // метод сортировки
    void SortData(void);                                    // сортировка данных
    void InsertSort(PTTabRecord *pMem, int DataCount);      // метод вставок
    void MergeSort(PTTabRecord *pMem, int DataCount);       // метод слияния
    void MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size);
    void MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2);
    void QuickSort(PTTabRecord *pMem, int DataCount);        // быстрая сортировка
    void QuickSplit(PTTabRecord *pData, int Size, int &Pivot);
public:
    TSortTable(int Size = TabMaxSize) : TScanTable(Size) {};    // конструктор
    TSortTable(const TScanTable &tab);                          // из просматриваемой таблицы
    TSortTable & operator=(const TScanTable &tab);              // присваивание
    TSortMethod GetSortMethod(void);                            // получить метод сортировки
    void SetSortMethod(TSortMethod sm);                         // установить метод сортировки

                                                                // основные методы
    virtual PTDatValue FindRecord(TKey k);                      // найти запись
    virtual void InsRecord(TKey k, PTDatValue pVal);            // вставить

};
```
#### Реализация (cpp-файл):
```
#include "../include/TSortTable.h"

TSortTable::TSortTable(const TScanTable &tab) : TScanTable(tab.TabSize) {
    CurrPos = tab.CurrPos;
    DataCount = tab.DataCount;
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = (PTTabRecord)(tab.pRecs[i]->GetCopy());
}

TSortTable & TSortTable::operator=(const TScanTable &tab) {
    delete[] tab.pRecs;
    pRecs = new PTTabRecord[tab.TabSize];
    TabSize = tab.TabSize;
    CurrPos = tab.CurrPos;
    DataCount = tab.DataCount;
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = (PTTabRecord)(tab.pRecs[i]->GetCopy());
    return *this;
}

TSortMethod TSortTable::GetSortMethod(void) {
    return SortMethod;
}

void TSortTable::SetSortMethod(TSortMethod sm) {
    SortMethod = sm;
}

PTDatValue TSortTable::FindRecord(TKey k) {
    PTDatValue result = 0;
    if (DataCount) {
        int i, i1 = 0, i2 = DataCount - 1;
        while (i1 <= i2) {
            i = (i1 + i2) / 2;
            if (pRecs[i]->GetKey() == k) {
                result = pRecs[i]->GetValuePtr();
                break;
            }
            else if (pRecs[i]->GetKey() > k)
                i2 = i - 1;
            else
                i1 = i + 1;
        }
    }
    return result;
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal) {
    pRecs[DataCount++] = new TTabRecord(k, pVal);
    SortData();
}

void TSortTable::SortData(void) {
    switch (SortMethod) {
    case QUICK_SORT:
        QuickSort(pRecs, DataCount);
        break;
    case INSERT_SORT:
        InsertSort(pRecs, DataCount);
        break;
    case MERGE_SORT:
        QuickSort(pRecs, DataCount);
        break;
    default:
        InsertSort(pRecs, DataCount);
        break;
    }
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount) {
    int j;
    PTTabRecord temp;
    for (int i = 0; i < DataCount; i++) {
        j = i;
        while (j > 0 && pMem[j]->GetKey() < pMem[j - 1]->GetKey()) {
            temp = pMem[j];
            pMem[j] = pMem[j - 1];
            pMem[j - 1] = temp;
            j--;
        }
    }
}

void TSortTable::MergeSort(PTTabRecord *pMem, int DataCount) {
    PTTabRecord *pData = pRecs;
    PTTabRecord *pBuff = new PTTabRecord[DataCount];
    PTTabRecord *pTemp = pBuff;
    MergeSorter(pData, pBuff, DataCount);
    if (pData == pTemp)
        for (int i = 0; i < DataCount; i++)
            pBuff[i] = pData[i];
    delete pTemp;
}

void TSortTable::MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size) {
    int n1 = (Size + 1) / 2;
    int n2 = Size - n1;
    if (Size > 2) {
        PTTabRecord *pData2 = pData + n1, *pBuff2 = pBuff + n1;
        MergeSorter(pData, pBuff, n1);
        MergeSorter(pData2, pBuff2, n2);
    }
    MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2) {
    for (int i = 0; i < (n1 + n2); i++)
        pBuff[i] = pData[i];
    PTTabRecord *&tmp = pData;
    pData = pBuff;
    pBuff = tmp;
}

void TSortTable::QuickSort(PTTabRecord *pMem, int DataCount) {
    int pivot;
    int n1;
    int n2;
    if (DataCount > 1) {
        QuickSplit(pRecs, DataCount, pivot);
        n1 = pivot + 1;
        n2 = DataCount - n1;
        QuickSort(pRecs, n1 - 1);
        QuickSort(pRecs + n1, n2);
    }
}

void TSortTable::QuickSplit(PTTabRecord *pData, int Size, int &Pivot) {
    PTTabRecord pPivot = pData[0];
    PTTabRecord pTemp;
    int i1 = 1;
    int i2 = Size - 1;
    while (i1 <= i2) {
        while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey()))
            i1++;
        while (pData[i2]->GetKey() > pPivot->GetKey())
            i2--;
        if (i1 < i2) {
            pTemp = pData[i1];
            pData[i1] = pData[i2];
            pData[i2] = pTemp;
        }
    }
    pData[0] = pData[i2];
    pData[i2] = pPivot;
    Pivot = i2;
}
```

***
### 9. Разработка и реализация класса  `TTreeNode`:
***
#### Объявление (h-файл):
```
#pragma once

#include "../include/TDatacom.h"
#include "../include/TTabRecord.h"

class TTreeNode;
typedef TTreeNode *PTTreeNode;

class TTreeNode : public TTabRecord {
protected:
	PTTreeNode pLeft, pRight; // указатели на поддеревья

public:
	TTreeNode(TKey k = "", PTDatValue pVal = nullptr, PTTreeNode pL = nullptr,
		PTTreeNode pR = nullptr) : TTabRecord(k, pVal), pLeft(pL), pRight(pR) {};
	PTTreeNode GetLeft(void) const;  // указатель на левое поддерево
	PTTreeNode GetRight(void) const; // указатель на правое поддерево
	virtual TDatValue * GetCopy() override;  // изготовить копию

	friend class TTreeTable;
	friend class TBalanceTree;
};
```
#### Реализация (cpp-файл):
```
#include "../include/TTreeNode.h"

PTTreeNode TTreeNode::GetLeft(void) const
{
	return pLeft;
}


PTTreeNode TTreeNode::GetRight(void) const
{
	return pRight;
}

TDatValue * TTreeNode::GetCopy()
{
	TTreeNode *tmp = new TTreeNode(Key, pValue, nullptr, nullptr);
	return tmp;
}
```

***
### 10. Разработка и реализация класса  `TTreeTable`:
***
#### Объявление (h-файл):
```
#pragma once

#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;			// указатель на корень дерева
	PTTreeNode *ppRef;			// адрес указателя на вершину-результата в FindRecord
	PTTreeNode pCurrent;		// указатель на текущую вершину
	int CurrPos;				// номер текущей вершины
	std::stack < PTTreeNode> St;		// стек для итератора
	void DeleteTreeTab(PTTreeNode pNode); // удаление
public:
	TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = NULL; ppRef = NULL; }
	~TTreeTable() { DeleteTreeTab(pRoot); }				// деструктор

																								// информационные методы
	virtual bool IsFull() const override;						// таблица заполнена?

																									//основные методы
	virtual PTDatValue FindRecord(TKey k) override;				// найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) override; 	// вставить
	virtual void DelRecord(TKey k) override;					// удалить запись

																										// навигация
	virtual TKey GetKey(void) const override;
	virtual PTDatValue GetValuePtr(void) const override;
	virtual void Reset(void) override;					// установить на первую запись
	virtual bool IsTabEnded(void) const override;		// таблица завершена?
	virtual int GoNext(void) override;					// переход к следующей записи
};


```
#### Реализация (cpp-файл):
```
#include "../include/TTreeTable.h"
#include "../include/TTreeNode.h"

bool TTreeTable::IsFull() const
{
	return false;
}

PTDatValue TTreeTable::FindRecord(TKey k) {
	PTTreeNode tmp = pRoot;
	ppRef = &pRoot;
	while (tmp != nullptr) {
		if (tmp->GetKey() == k) break;
		if (tmp->GetKey() < k) ppRef = &tmp->pRight;
		else ppRef = &tmp->pLeft;
		tmp = *ppRef;
	}
	if (tmp == nullptr) {
		SetRetCode(TabNoRec);
		return nullptr;
	}
	else {
		SetRetCode(TabOK);
		return tmp->GetValuePtr();
	}
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(TabNoMem);
	}
	else {
		if (FindRecord(k) != nullptr) {
			SetRetCode(TabNoRec);
		}
		else {
			*ppRef = new TTreeNode(k, pVal);
			DataCount++;
		}
	}
}

void TTreeTable::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(TabNoRec);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}
		// удаление листа
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					else if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (справа)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (слева)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с двумя потомками
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}

	}
}

TKey TTreeTable::GetKey(void) const
{
	return (pCurrent == nullptr) ? "" : pCurrent->GetKey();
}

PTDatValue TTreeTable::GetValuePtr(void) const
{
	return (pCurrent == nullptr) ? nullptr : pCurrent->GetValuePtr();
}

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
	if (pNode != nullptr) {
		DeleteTreeTab(pNode->GetLeft());
		DeleteTreeTab(pNode->GetRight());
		delete pNode;
	}
}

void TTreeTable::Reset(void)
{
	PTTreeNode pNode = pCurrent = pRoot;
	CurrPos = 0;
	while (pNode != nullptr) {
		St.push(pNode);
		pCurrent = pNode;
		pNode = pNode->GetLeft();
	}
	SetRetCode(TabOK);
}

bool TTreeTable::IsTabEnded(void) const
{
	return (CurrPos >= DataCount);
}

int TTreeTable::GoNext(void)
{
	CurrPos++;
	if (!IsTabEnded() && (pCurrent != nullptr)) {
		PTTreeNode pNode = pCurrent = pCurrent->GetRight();
		St.pop();
		while (pNode != nullptr) {
			St.push(pNode);
			pCurrent = pNode;
			pNode = pNode->GetLeft();
		}
		if ((pCurrent == nullptr) && !St.empty())
			pCurrent = St.top();

	}
	else SetRetCode(TabNoRec);
	return GetRetCode();
}
```

***
### 11. Разработка и реализация класса  `TBalanceNode`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TTreeNode.h"

enum class Bal { BalOK, BalLeft, BalRight };

class  TBalanceNode : public TTreeNode {
protected:
	Bal Balance; // индекс балансировки вершины
public:
	TBalanceNode(TKey k = "", PTDatValue pVal = NULL, PTTreeNode pL = NULL,
		PTTreeNode pR = NULL, Bal bal = Bal::BalOK) : TTreeNode(k, pVal, pL, pR),
		Balance(bal) {};			// конструктор
	virtual TDatValue * GetCopy();  // изготовить копию
	Bal GetBalance(void) const;
	void SetBalance(Bal bal);
	friend class TBalanceTree;
};

typedef TBalanceNode *PTBalanceNode;

```
#### Реализация (cpp-файл):
```
#include "../include/TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
	TBalanceNode *tmp = new TBalanceNode(Key, pValue, nullptr, nullptr, Bal::BalOK);
	return tmp;
}

Bal TBalanceNode::GetBalance(void) const
{
	return Balance;
}

void TBalanceNode::SetBalance(Bal bal)
{
	Balance = bal;
}
```

***
### 12. Разработка и реализация класса  `TBalanceTree`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TTreeTable.h"
#include "TBalanceNode.h"

enum class Height { OK, Inc };

class  TBalanceTree : public TTreeTable {
protected:
	Height InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	Height LeftTreeBalancing(PTBalanceNode &pNode);  // баланс. левого поддерева
	Height RightTreeBalancing(PTBalanceNode &pNode); // баланс. правого поддерева
public:
	TBalanceTree() :TTreeTable() {} // конструктор
																	//основные методы
	virtual void InsRecord(TKey k, PTDatValue pVal) override final; // вставить
	virtual void DelRecord(TKey k) override final;                  // удалить
};

typedef TBalanceTree *PTBalanceTree;

```
#### Реализация (cpp-файл):
```
#include "../include/TBalanceTree.h"

Height TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
	Height HeightIndex = Height::OK;
	if (pNode == nullptr) { // вставка вершины
		pNode = new TBalanceNode(k, pVal);
		HeightIndex = Height::Inc;
		DataCount++;
	}
	else if (k < pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pLeft, k, pVal) == Height::Inc) {
			HeightIndex = LeftTreeBalancing(pNode);
		}
	}
	else if (k > pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pRight, k, pVal) == Height::Inc) {
			HeightIndex = RightTreeBalancing(pNode);
		}
	}
	else {
		SetRetCode(TabNoRec);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalRight:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalLeft);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalLeft:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetLeft());
		if (p1->GetBalance() == Bal::BalLeft) {
			pNode->pLeft = p1->pRight;
			p1->pRight = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetRight());
			p1->pRight = p2->pLeft;
			p2->pLeft = p1;
			pNode->pLeft = p2->pRight;
			p2->pRight = pNode;
			if (p2->GetBalance() == Bal::BalLeft) {
				pNode->SetBalance(Bal::BalRight);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalRight) {
				p1->SetBalance(Bal::BalLeft);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalLeft:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalRight);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalRight:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetRight());
		if (p1->GetBalance() == Bal::BalRight) {
			pNode->pRight = p1->pLeft;
			p1->pLeft = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetLeft());
			p1->pLeft = p2->pRight;
			p2->pRight = p1;
			pNode->pRight = p2->pLeft;
			p2->pLeft = pNode;
			if (p2->GetBalance() == Bal::BalRight) {
				pNode->SetBalance(Bal::BalLeft);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalLeft) {
				p1->SetBalance(Bal::BalRight);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}


void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(TabNoMem);
	}
	else {
		InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
	}
}

void TBalanceTree::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(TabNoRec);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}

		TKey k2 = tmp->GetKey();
		// удаление листа
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (справа)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (слева)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с двумя потомками
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		if (pRoot != nullptr) {
			if (k2 < pRoot->GetKey()) {
				LeftTreeBalancing((PTBalanceNode&)pRoot);
			}
			else  if (k2 > pRoot->GetKey()) {
				RightTreeBalancing((PTBalanceNode&)pRoot);
			}
		}
	}
}
```

***
### 13. Разработка и реализация класса  `THashTable`:
***
#### Объявление (h-файл):
```
#pragma once

#include "TTable.h"

class  THashTable : public TTable {
protected:
    unsigned long HashFunc(const TKey& key);
public:
    static const int TAB_MAX_SIZE = 25;
    THashTable() : TTable() {}
};
```
#### Реализация (cpp-файл):
```
#include "../include/THashTable.h"

unsigned long THashTable::HashFunc(const TKey& key)
{
    unsigned long hashval = 0;
    int Len = key.length();
    for (int i = 0; i < Len; i++)
        hashval = (hashval << 3) + key.at(i);
    return hashval;
}
```

***
### 14. Разработка и реализация класса  `TArrayHash`:
***
#### Объявление (h-файл):
```
#pragma once

#pragma once

#include "THashTable.h"
#include "TTabRecord.h"

class  TArrayHash : public THashTable {
protected:
    PTTabRecord* pRecs;                                              // память для записей таблицы
    int TabSize;                                                     // макс. возм. к-во записей
    int HashStep;                                                    // шаг вторичного перемешивания
    int CurrPos;                                                     // строка памяти при завершении поиска
    int GetNextPos(int pos) { return (pos + HashStep) % TabSize; };  // откр. перем.
public:
    // константы
    static const int TAB_HASH_STEP = 1;
    TArrayHash(int size = TAB_MAX_SIZE);
    ~TArrayHash();
    // информационные методы
    virtual bool IsFull() const;                       // заполнена?
                                                      // доступ
    virtual TKey GetKey(void) const;
    virtual PTDatValue GetValuePtr(void) const;
    // основные методы
    virtual PTDatValue FindRecord(TKey k);             // найти запись
    virtual void InsRecord(TKey k, PTDatValue pVal);   // вставить
    virtual void DelRecord(TKey k);                    // удалить запись
                                                       // навигация
    virtual void Reset(void);                          // установить на первую запись
    virtual bool IsTabEnded(void) const;               // таблица завершена?
    virtual int GoNext(void);                         // переход к следующей записи

};
```
#### Реализация (cpp-файл):
```
#include "../include/TArrayHash.h"

#include <iostream>

TArrayHash::TArrayHash(int size) : THashTable() {
    TabSize = size;
    HashStep = 1;
    pRecs = new PTTabRecord[TabSize];
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = 0;
    DataCount = 0;
}

TArrayHash::~TArrayHash() {
    delete[] pRecs;
}

bool TArrayHash::IsFull() const {
    return DataCount >= TabSize;
}

TKey TArrayHash::GetKey(void) const {
    return pRecs[CurrPos]->GetKey();
}

PTDatValue TArrayHash::GetValuePtr(void) const {
    if (pRecs[CurrPos] != 0)
        return pRecs[CurrPos]->GetValuePtr();
    else
        return 0;
}

PTDatValue TArrayHash::FindRecord(TKey k) {
    CurrPos = HashFunc(k) % TabSize;
    for (int i = 0; i < TabSize; i++)
    {
        Efficiency++;
        if (pRecs[CurrPos] == 0)
            break;
        else if (pRecs[CurrPos]->GetKey() == k)
            return pRecs[CurrPos]->GetValuePtr();
        CurrPos = GetNextPos(CurrPos);
    }
    SetRetCode(TabNoRec);
    return 0;
}

void TArrayHash::InsRecord(TKey k, PTDatValue pVal) {
    CurrPos = HashFunc(k) % TabSize;
    for (int i = 0; i < TabSize; i++)
    {
        Efficiency++;
        if (pRecs[CurrPos] != 0 && pRecs[CurrPos]->GetKey() == k)
            return;
        else if (pRecs[CurrPos] == 0)
        {
            pRecs[CurrPos] = new TTabRecord(k, pVal);
            DataCount++;
            return;
        }
        CurrPos = GetNextPos(CurrPos);
    }
    SetRetCode(TabNoMem);
}

void TArrayHash::DelRecord(TKey k) {
    PTDatValue record = FindRecord(k);
    if (record != 0)
    {
        delete pRecs[CurrPos];
        pRecs[CurrPos] = 0;
        DataCount--;
    }
    else
        SetRetCode(TabNoRec);
}

void TArrayHash::Reset(void) {
    CurrPos = 0;
    GoNext();
}

bool TArrayHash::IsTabEnded(void) const {
    return CurrPos >= TabSize;
}

int TArrayHash::GoNext(void) {
    if (IsTabEnded())
        SetRetCode(TabNoRec);
    else
        while (++CurrPos < TabSize)
            if (pRecs[CurrPos] != 0)
                break;
    return GetRetCode();
}
```

***
### 15. Разработка и реализация класса  `TTableViewer`:
***
#### Объявление (h-файл):
```
#pragma once

#include "../include/TScanTable.h"
#include "../include/TSortTable.h"
#include "../include/TArrayHash.h"
#include "../include/TTreeTable.h"

#include <iostream>
#include <fstream>
#include <string>
#include <conio.h>
#include <vector>

using namespace std;

class TTableViewer {
public:
    TTableViewer() { };
    void PushMessage(string message);

private:

    enum UsingTable { Scan, Sort, Hash, Tree };

    UsingTable Use = Scan;
    TTable* Table = new TScanTable();
    vector<string> arg;
    string FilePath = "";

    static int StrToInt(string str);
    vector<string> MessageToArgs(string message);

    void Help();
    void Delete(string str);
    void Find(string str);
    void Insert(string str);
    void Replace(string str);

    double AverageMarks();
    string BestStudent();
    double AverageMarkOfSubject(int NumOfSub);
    void InformationAboutStudent(string student);
    void HighAchievers();

    void Save(string str);
    void Load(string str);

    void Show();
};
```
#### Реализация (cpp-файл):
```
#include "../include/TTableViewer.h"
#include "../include/Marks.h"

void TTableViewer::PushMessage(string message) {

    arg = MessageToArgs(message);
    if (arg.size() == 0)
        throw "you dont write any command";
    else if (arg.size() == 1)
    {
        if (arg.at(0) == "sort") {
            Use = Sort;
            delete Table;
            Table = new TSortTable();
            FilePath = "";
        }
        else if (arg.at(0) == "scan") {
            Use = Scan;
            delete Table;
            Table = new TScanTable();
            FilePath = "";
        }
        else if (arg.at(0) == "hash") {
            Use = Hash;
            delete Table;
            Table = new TArrayHash();
            FilePath = "";
        }
        else if (arg.at(0) == "tree") {
					Use = Tree;
					delete Table;
					Table = new TTreeTable();
					FilePath = "";
        }
        else if (arg.at(0) == "save" && FilePath != "")
            Save(FilePath);
        else if (arg.at(0) == "show")
            Show();
        else if (arg.at(0) == "help")
            Help();
        else if (arg.at(0) == "avrg")
            cout << AverageMarks() << endl;
        else if (arg.at(0) == "best")
            cout << BestStudent() << endl;
        else if (arg.at(0) == "highach")
            HighAchievers();
    }
    else if (arg.size() == 2)
    {
        if (arg.at(0) == "delete")
            Delete(arg.at(1));
        else if (arg.at(0) == "find")
            Find(arg.at(1));
        else if (arg.at(0) == "save")
            Save(arg.at(1));
        else if (arg.at(0) == "load")
            Load(arg.at(1));
        else if (arg.at(0) == "avrgofsub")
            cout << AverageMarkOfSubject(StrToInt(arg.at(1))) << endl;
        else if (arg.at(0) == "studinf")
            InformationAboutStudent(arg.at(1));
    }
    else if (arg.size() > 2)
    {
        if (arg.at(0) == "insert")
            Insert(arg.at(1));
        if (arg.at(0) == "replace")
            Replace(arg.at(1));
    }
        
    else
        throw "incorrect command or arguments, write help to show abilities of programm";

}

vector<string> TTableViewer::MessageToArgs(string message) {
    vector<string> result;
    bool IsIntoString = false;
    string buffstring = "";

    for (int i = 0; i < message.size(); i++) {
        if (message.at(i) == ' ' && !IsIntoString && buffstring.size() > 0) {
            result.push_back(buffstring);
            buffstring.clear();
        }
        else if (message.at(i) == '"')
            IsIntoString = !IsIntoString;
        else if (message.at(i) != ' ' || IsIntoString)
            buffstring.push_back(message.at(i));
    }
    if (buffstring.size() > 0)
        result.push_back(buffstring);

    return result;
}

int TTableViewer::StrToInt(string str) {
    int result = 0;

    for (int i = str.size() - 1, f = 0; i >= 0; i--, f++)
        if (str.at(i) <= '9' && str.at(i) >= '0')
            result = (str.at(i) - '0') * pow(10, f);
        else
            throw ("error: incorrect char of one of argument: " + str.at(i));

    return result;
}

void TTableViewer::Delete(string str) {
    Table->DelRecord(str);
    if (Table->GetRetCode() == TabNoRec)
        throw "No record";
}

void TTableViewer::Find(string str) {
    Marks* Mark = (Marks*)Table->FindRecord(str);
    if (Table->GetRetCode() == TabNoRec)
        throw "No record";
    else {
        cout << str << " ";
        for (int i = 0; i < Subjects; i++)
            cout << Mark->Values[i] << " ";
        cout << endl;
    }
}

void TTableViewer::Insert(string str) {
    Marks* Mark = new Marks();
    if (arg.size() - 2 == Subjects)
        for (int i = 0; i < Subjects; i++)
            Mark->Values[i] = StrToInt(arg.at(i + 2));
    else
        throw "incorrect arguments";
    Table->InsRecord(str, (TDatValue*)Mark);
    if (Table->GetRetCode() == TabNoMem)
        throw "Not enough memory";
}

void TTableViewer::Replace(string str) {
    Marks* Replacement = new Marks();
    if (arg.size() - 2 == Subjects)
        for (int i = 0; i < Subjects; i++)
            Replacement->Values[i] = StrToInt(arg.at(i + 2));
    else
        throw "incorrect arguments";
    Marks* Mark = (Marks*)Table->FindRecord(str);
    if (Table->GetRetCode() == TabNoRec)
        throw "No record";
    else
        for (int i = 0; i < Subjects; i++)
            Mark->Values[i] = Replacement->Values[i];
}

void TTableViewer::Save(string str) {
    ofstream strm(str);
    string Key;
    Marks* Mark;
    if (strm.is_open()) {
        Table->Reset();
        for (int i = 0; i < Table->GetDataCount(); i++)
        {
            Key = Table->GetKey();
            Mark = (Marks*)Table->GetValuePtr();
            strm << "\"" <<  Key << "\"" << " ";
            for (int f = 0; f < Subjects; f++)
                strm << Mark->Values[f] << " ";
            strm << endl;
            Table->GoNext();
        }
    }
    else
        throw "cant open the file";
    strm.close();
}

void TTableViewer::Load(string str) {
    ifstream strm(str);
    string line;
    vector<string> args;
    string Key;
    TTable* TableBuffer;

    switch (Use) {
		case Tree: TableBuffer = new TTreeTable(); break;
    case Sort: TableBuffer = new TSortTable(); break;
    case Hash: TableBuffer = new TArrayHash(); break;
    default: TableBuffer = new TScanTable(); break;
    }

    if (strm.is_open()) {
        while (!strm.eof()) {
            getline(strm, line);
            if (line != "") {
                Marks* Mark = new Marks();
                args = MessageToArgs(line);
                if (args.size() != Subjects + 1)
                    throw "incorrect file input, check file to resolve the problem";
                Key = args.at(0);
                for (int i = 0; i < Subjects; i++)
                    Mark->Values[i] = StrToInt(args.at(i + 1));
                TableBuffer->InsRecord(Key, (PTDatValue)Mark);
                if (TableBuffer->GetRetCode() != TabOK)
                    throw "something going wrong";
            }
        }
        delete Table;
        Table = TableBuffer;
        line.clear();
    }
    else
        throw "cant open the file";

    strm.close();
}

void TTableViewer::Show() {

    Marks* Mark;

    Table->Reset();

    for (int i = 0; i < Table->GetDataCount(); i++) {
        cout << Table->GetKey() << " ";
        Mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++)
            cout << Mark->Values[f] << " ";
        cout << endl;
        Table->GoNext();
    }
}

double TTableViewer::AverageMarks() {
    int avrg = 0;
    Table->Reset();
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++)
            avrg += mark->Values[f];
        Table->GoNext();
    }
    if (Table->GetDataCount() != 0)
        return (double)avrg / (double)(Subjects*Table->GetDataCount());
    else
        return avrg;
}

string TTableViewer::BestStudent() {
	  Table->Reset();
    int avrg1 = 0;
    int avrg2 = 0;
    string name("");
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++)
            avrg2 += mark->Values[f];
        if (avrg2 > avrg1) {
            name = Table->GetKey();
            avrg1 = avrg2;
        }
        avrg2 = 0;
        Table->GoNext();
    }
    return name;
}

double TTableViewer::AverageMarkOfSubject(int NumOfSub) {
    int avrg = 0;
    Table->Reset();
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        avrg += mark->Values[NumOfSub];
        Table->GoNext();
    }
    if (Table->GetDataCount() != 0)
        return (double)avrg / (double)(Table->GetDataCount());
    else
        return avrg;
}

void TTableViewer::InformationAboutStudent(string student) {
    Marks* mark = (Marks*)Table->FindRecord(student);
    if (Table->GetRetCode() == TabNoRec)
        throw "This student dont exist";
    else {
        int avrg = 0;
        cout << "Marks: ";
        for (int i = 0; i < Subjects; i++) {
            cout << mark->Values[i] << " ";
            avrg += mark->Values[i];
        }
        cout << endl;
        cout << "Average of marks: " << (double)avrg/ (double)Subjects << endl;
    }
}

void TTableViewer::HighAchievers() {
    Marks* mark;
    bool Good = true;
    Table->Reset();
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++) {
            if (mark->Values[f] != 5)
                Good = false;
        }
        if (Good)
            cout << Table->GetKey() << endl;
        Good = true;
        Table->GoNext();
    }
}

void TTableViewer::Help() {
    cout << "commands: " << endl;
    cout << "save, save \"path\", load \"path\" - save or load table" << endl;
    cout << "<Marks> means sequence of mark, like <mark1> <mark2> ... <markN>" << endl;
    cout << "insert/replace \"key\" <Marks> - insert or replace marks" << endl;
    cout << "delete \"key\" - delete string" << endl;
    cout << "show - print table on console" << endl;
    cout << "avrg - average of marks" << endl;
    cout << "best - best student" << endl;
    cout << "highach - list of high achievers" << endl;
    cout << "studinf \"name\" - information about student" << endl;
    cout << "avrgofsub <NumberOfSubject> - average mark of one subject" << endl;
    cout << "exit, quit, q - quit the program" << endl;
    cout << "subjects: " << endl;
    cout << "0 - AlgorithmsAndStructureOfData, 1 - OptimizationMetods, 2 - OperationSystems" << endl;
    cout << "3 - DevelopingPO, 4 - DevelopingInstruments" << endl;
    cout << "Credits: " << endl;
    cout << "Table editor by Aleksandr Nazarov and Mihail Gorozhanin" << endl;
}
```

***
### 16.Разработка и реализация тестовой программы `TTableTestkit.cpp`:
***
#### Реализация (cpp-файл):
```
#include "../include/TTableViewer.h"
#include <iostream>

int main() {
    TTableViewer Viewer;
    string command = "";
    while (command != "quit" && command != "q" && command != "exit") {
        try {
            command = "";
            getline(cin, command);
            Viewer.PushMessage(command);
        }
        catch (char* err) {
            cout << err << endl;
        }
        catch (string err) {
            cout << err << endl;
        }
    }
    return 0;
}
```
***
###17. Тесты для проверки  классов (GoogleTestFramework)
***
#####**Test_gtest.cpp**
```c++
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <gtest/gtest.h>
#include "TArrayTable.h"
#include "../src/TArrayTable.cpp"
#include "TScanTable.h"
#include "TSortTable.h"
#include "../src/TScanTable.cpp"
#include "../src/TSortTable.cpp"
#include <gtest/gtest.h>
#include <TTreeNode.h>
#include <../src/TTreeNode.cpp>
#include <TBalanceNode.h>
#include <../src/TBalanceNode.cpp>
#include "TTreeTable.h"
#include "../src/TTreeTable.cpp"
#include "TBalanceTree.h"
#include "../src/TBalanceTree.cpp"
#include "Marks.h"

TEST(ScanTable, Can_Create_ScanTable_Number) {
	ASSERT_NO_THROW(TScanTable table(5));
}

TEST(ScanTable, Can_Insert_Record) {
	TScanTable* table = new TScanTable(5);
	int i = 123;

	ASSERT_NO_THROW((table->InsRecord("abc", (TDatValue*)&i)));
}

TEST(ScanTable, Can_Get_Table_Size) {
	TScanTable* table = new TScanTable(5);

	EXPECT_EQ(5, table->GetTabSize());
}

TEST(TScanTable, can_create_table) {
	ASSERT_NO_THROW(TScanTable testing_table);
}

TEST(TScanTable, cant_create_table_with_incorrect_size) {
	ASSERT_ANY_THROW(TScanTable testing_table(-1));
}

TEST(TScanTable, can_insert_record) {
	TScanTable testing_table;
	int Test_Marks[Subjects];
	for (int i = 0; i < Subjects; i++)
		Test_Marks[i] = i;
	TDatValue* Data = new Marks(Test_Marks);
	Marks* Mark;
	testing_table.InsRecord("test_key", Data);
	Mark = (Marks*)testing_table.FindRecord("test_key");
	EXPECT_TRUE(Mark->Values[2] == 2);
}

TEST(ScanTable, Full_Table_Is_Full) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}

	ASSERT_TRUE(table->IsFull());
}

TEST(ScanTable, Can_Set_Current_Position) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->SetCurrentPos(2);

	ASSERT_TRUE("aaac" == table->GetKey());
}

TEST(ScanTable, Can_Get_Current_Position) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->SetCurrentPos(3);

	EXPECT_EQ(3, table->GetCurrentPos());
}

TEST(ScanTable, Can_Reset_Position) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->SetCurrentPos(3);
	table->Reset();

	EXPECT_EQ(0, table->GetCurrentPos());
}

TEST(ScanTable, Can_Go_Next_Record) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}
	table->GoNext();
	table->GoNext();

	bool correctness = (2 == table->GetCurrentPos()) &&
		("aaac" == table->GetKey());
	ASSERT_TRUE(correctness);
}

TEST(ScanTable, Can_Find_Record) {
	TScanTable* table = new TScanTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->InsRecord("aaaa", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("aaab", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("aaba", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("aabc", (TDatValue*)i);

	EXPECT_EQ(2, *((int*)(table->FindRecord("aaab"))));
}

TEST(ScanTable, Can_Delete_Record) {
	TScanTable* table = new TScanTable(5);
	int numbers[5];
	string s = "aaa";

	for (int i = 0; i < 5; i++) {
		char smb = 'a' + i;
		numbers[i] = i;
		table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
	}

	ASSERT_NO_THROW(table->DelRecord("aaac"));
	EXPECT_TRUE(nullptr == table->FindRecord("aaac"));
}

TEST(SortTable, Can_Use_Different_Sort_Methods) {
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaaa", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaab", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 3;
	table->SetSortMethod(QUICK_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaac", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	ASSERT_NO_THROW(table->InsRecord("aaad", (TDatValue*)&i[0]));

	EXPECT_EQ(3, *((int*)(table->FindRecord("aaac"))));
}
TEST(TabRecord, Can_Create_TabRecord) {
	ASSERT_NO_THROW(TTabRecord rec1("abc"));
}

TEST(TabRecord, Can_Set_Key) {
	TTabRecord rec1;
	ASSERT_NO_THROW(rec1.SetKey("abc"));
}

TEST(TabRecord, Can_Get_Key) {
	string s = "cba";
	TTabRecord rec1("abc");

	rec1.SetKey(s);

	bool keys_equality = (s == rec1.GetKey()) && (rec1.GetKey() != "abc");

	ASSERT_TRUE(keys_equality);
}

TEST(TabRecord, Can_Set_Value) {
	TTabRecord rec1("abc");
	int val = 123;

	ASSERT_NO_THROW(rec1.SetValuePtr((PTDatValue)&val));
}

TEST(TabRecord, Can_Get_Value) {
	TTabRecord rec1("abc");
	int val = 123, expected_val;

	rec1.SetValuePtr((PTDatValue)&val);

	expected_val = *(int*)(rec1.GetValuePtr());

	EXPECT_EQ(val, expected_val);
}

TEST(TabRecord, Records_With_Equal_Keys_Are_Equal) {
	TTabRecord rec1("abc");
	TTabRecord rec2("abc");

	ASSERT_TRUE(rec1 == rec2);
}

TEST(TabRecord, Records_With_Not_Equal_Keys_Are_Not_Equal) {
	TTabRecord rec1("abc");
	TTabRecord rec2("cba");

	ASSERT_FALSE(rec1 == rec2);
}

TEST(TabRecord, Record_With_Bigger_Key_Is_Less) {
	TTabRecord rec1("aaa"), rec2("aab");

	ASSERT_TRUE(rec2 > rec1);
}

TEST(TabRecord, Record_With_Less_Key_Is_Less) {
	TTabRecord rec1("aaa"), rec2("aab");

	ASSERT_TRUE(rec1 < rec2);
}

TEST(TabRecord, Can_Assign_Record) {
	TTabRecord rec1("abc"), rec2;

	rec2 = rec1;

	ASSERT_TRUE(rec1 == rec2);
}

TEST(TabRecord, Assigned_Record_Has_Its_own_Mem) {
	TTabRecord rec1("abc"), rec2;

	rec2 = rec1;
	rec2.SetKey("cba");

	ASSERT_FALSE(rec1 == rec2);
}
TEST(TreeNode, Can_Create_Tree_Node) {
	ASSERT_NO_THROW(TTreeNode node());
}

TEST(TreeNode, Can_Place_Nodes_In_Tree) {
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("aaaa", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("aaac", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("aaab", (PTDatValue)&i[0], &left, &right);

	EXPECT_EQ(Center.GetLeft(), &left);
	EXPECT_EQ(Center.GetRight(), &right);
}

TEST(BalanceNode, Can_Create_Balanced_Tree_Node) {
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TreeTable, Can_Create_Tree_Table) {
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TreeTable, Can_Insert_Record) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	ASSERT_NO_THROW(table->InsRecord("aaaa", (PTDatValue)&i[0]));
}

TEST(TreeTable, Can_Get_Key) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("aaaa", (PTDatValue)&i[0]);
	table->Reset();
	EXPECT_EQ("aaaa", table->GetKey());
}

TEST(TreeTable, Can_Get_Value) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("aaaa", (PTDatValue)&i[0]);
	table->Reset();
	EXPECT_EQ(3, *((int*)table->GetValuePtr()));
}

TEST(TreeTable, Can_Find_Record) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("aaaa", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("aaae", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("aaad", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("aaae"), 5);
}

TEST(BalanceTree, Can_Create_Balanced_Tree) {
	ASSERT_NO_THROW(TBalanceTree tree);
}

TEST(BalanceTree, Can_Add_And_Find_Record) {
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("aaai", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("aaba", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("acaa", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("aaab", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("aage", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("aaab"), 2);
}

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
```

***
### 18. Обеспечение работоспособности тестов:
***
![](image/Test1.png)
![](image/Test2.png)
***
###19. Результат работы программы:
![](image/Work.png)

***
## Вывод:
***
##### Все цели и задачи работы достигнуты. В ходе выполнения данной работы, нам удалось разработать и реализовать все классы перечисленные в пункте "Структура проекта"

##### Также были написаны тесты к данному проекту под систему тестирования `Google Test`, что позволяет не только проверить правильность работы программы, но и также лучше ознакомиться с `Google Test Framework`. Успешное же прохождение всех тестов показывает, что классы были реализованы корректно и программа созданная с использованием этих классов работает верно. 
##### Особенностью данной работы было то, что:

1. Были освоены такие структуры как  **"Бинарное дерево"**, а также **"Хеш-Таблица"** , которые могут быть использованы в дальнейших работах (или для решения любых других практических задач)
2. Реализация данных структур была осуществлена изначально без использования предыдущего опыта(за исключением `TDatValue` и переработанного `tdatacom`)
3. Также на основе данных структур была создана программа для работы с таблицами. 
4. Работа проходила в командном виде (над одним "проектом" работали двое).

