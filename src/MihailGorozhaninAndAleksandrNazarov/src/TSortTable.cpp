#include "../include/TSortTable.h"

TSortTable::TSortTable(const TScanTable &tab) : TScanTable(tab.TabSize) {
    CurrPos = tab.CurrPos;
    DataCount = tab.DataCount;
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = (PTTabRecord)(tab.pRecs[i]->GetCopy());
}

TSortTable & TSortTable::operator=(const TScanTable &tab) {
    delete[] tab.pRecs;
    pRecs = new PTTabRecord[tab.TabSize];
    TabSize = tab.TabSize;
    CurrPos = tab.CurrPos;
    DataCount = tab.DataCount;
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = (PTTabRecord)(tab.pRecs[i]->GetCopy());
    return *this;
}

TSortMethod TSortTable::GetSortMethod(void) {
    return SortMethod;
}

void TSortTable::SetSortMethod(TSortMethod sm) {
    SortMethod = sm;
}

PTDatValue TSortTable::FindRecord(TKey k) {
    PTDatValue result = 0;
    if (DataCount) {
        int i, i1 = 0, i2 = DataCount - 1;
        while (i1 <= i2) {
            i = (i1 + i2) / 2;
            if (pRecs[i]->GetKey() == k) {
                result = pRecs[i]->GetValuePtr();
                break;
            }
            else if (pRecs[i]->GetKey() > k)
                i2 = i - 1;
            else
                i1 = i + 1;
        }
    }
    return result;
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal) {
    pRecs[DataCount++] = new TTabRecord(k, pVal);
    SortData();
}

void TSortTable::SortData(void) {
    switch (SortMethod) {
    case QUICK_SORT:
        QuickSort(pRecs, DataCount);
        break;
    case INSERT_SORT:
        InsertSort(pRecs, DataCount);
        break;
    case MERGE_SORT:
        QuickSort(pRecs, DataCount);
        break;
    default:
        InsertSort(pRecs, DataCount);
        break;
    }
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount) {
    int j;
    PTTabRecord temp;
    for (int i = 0; i < DataCount; i++) {
        j = i;
        while (j > 0 && pMem[j]->GetKey() < pMem[j - 1]->GetKey()) {
            temp = pMem[j];
            pMem[j] = pMem[j - 1];
            pMem[j - 1] = temp;
            j--;
        }
    }
}

void TSortTable::MergeSort(PTTabRecord *pMem, int DataCount) {
    PTTabRecord *pData = pRecs;
    PTTabRecord *pBuff = new PTTabRecord[DataCount];
    PTTabRecord *pTemp = pBuff;
    MergeSorter(pData, pBuff, DataCount);
    if (pData == pTemp)
        for (int i = 0; i < DataCount; i++)
            pBuff[i] = pData[i];
    delete pTemp;
}

void TSortTable::MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size) {
    int n1 = (Size + 1) / 2;
    int n2 = Size - n1;
    if (Size > 2) {
        PTTabRecord *pData2 = pData + n1, *pBuff2 = pBuff + n1;
        MergeSorter(pData, pBuff, n1);
        MergeSorter(pData2, pBuff2, n2);
    }
    MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2) {
    for (int i = 0; i < (n1 + n2); i++)
        pBuff[i] = pData[i];
    PTTabRecord *&tmp = pData;
    pData = pBuff;
    pBuff = tmp;
}

void TSortTable::QuickSort(PTTabRecord *pMem, int DataCount) {
    int pivot;
    int n1;
    int n2;
    if (DataCount > 1) {
        QuickSplit(pRecs, DataCount, pivot);
        n1 = pivot + 1;
        n2 = DataCount - n1;
        QuickSort(pRecs, n1 - 1);
        QuickSort(pRecs + n1, n2);
    }
}

void TSortTable::QuickSplit(PTTabRecord *pData, int Size, int &Pivot) {
    PTTabRecord pPivot = pData[0];
    PTTabRecord pTemp;
    int i1 = 1;
    int i2 = Size - 1;
    while (i1 <= i2) {
        while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey()))
            i1++;
        while (pData[i2]->GetKey() > pPivot->GetKey())
            i2--;
        if (i1 < i2) {
            pTemp = pData[i1];
            pData[i1] = pData[i2];
            pData[i2] = pTemp;
        }
    }
    pData[0] = pData[i2];
    pData[i2] = pPivot;
    Pivot = i2;
}