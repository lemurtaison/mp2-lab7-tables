
#include "../include/TTableViewer.h"
#include "../include/Marks.h"

void TTableViewer::PushMessage(string message) {

    arg = MessageToArgs(message);
    if (arg.size() == 0)
        throw "you dont write any command";
    else if (arg.size() == 1)
    {
        if (arg.at(0) == "sort") {
            Use = Sort;
            delete Table;
            Table = new TSortTable();
            FilePath = "";
        }
        else if (arg.at(0) == "scan") {
            Use = Scan;
            delete Table;
            Table = new TScanTable();
            FilePath = "";
        }
        else if (arg.at(0) == "hash") {
            Use = Hash;
            delete Table;
            Table = new TArrayHash();
            FilePath = "";
        }
        else if (arg.at(0) == "tree") {
					Use = Tree;
					delete Table;
					Table = new TTreeTable();
					FilePath = "";
        }
        else if (arg.at(0) == "save" && FilePath != "")
            Save(FilePath);
        else if (arg.at(0) == "show")
            Show();
        else if (arg.at(0) == "help")
            Help();
        else if (arg.at(0) == "avrg")
            cout << AverageMarks() << endl;
        else if (arg.at(0) == "best")
            cout << BestStudent() << endl;
        else if (arg.at(0) == "highach")
            HighAchievers();
    }
    else if (arg.size() == 2)
    {
        if (arg.at(0) == "delete")
            Delete(arg.at(1));
        else if (arg.at(0) == "find")
            Find(arg.at(1));
        else if (arg.at(0) == "save")
            Save(arg.at(1));
        else if (arg.at(0) == "load")
            Load(arg.at(1));
        else if (arg.at(0) == "avrgofsub")
            cout << AverageMarkOfSubject(StrToInt(arg.at(1))) << endl;
        else if (arg.at(0) == "studinf")
            InformationAboutStudent(arg.at(1));
    }
    else if (arg.size() > 2)
    {
        if (arg.at(0) == "insert")
            Insert(arg.at(1));
        if (arg.at(0) == "replace")
            Replace(arg.at(1));
    }
        
    else
        throw "incorrect command or arguments, write help to show abilities of programm";

}

vector<string> TTableViewer::MessageToArgs(string message) {
    vector<string> result;
    bool IsIntoString = false;
    string buffstring = "";

    for (int i = 0; i < message.size(); i++) {
        if (message.at(i) == ' ' && !IsIntoString && buffstring.size() > 0) {
            result.push_back(buffstring);
            buffstring.clear();
        }
        else if (message.at(i) == '"')
            IsIntoString = !IsIntoString;
        else if (message.at(i) != ' ' || IsIntoString)
            buffstring.push_back(message.at(i));
    }
    if (buffstring.size() > 0)
        result.push_back(buffstring);

    return result;
}

int TTableViewer::StrToInt(string str) {
    int result = 0;

    for (int i = str.size() - 1, f = 0; i >= 0; i--, f++)
        if (str.at(i) <= '9' && str.at(i) >= '0')
            result = (str.at(i) - '0') * pow(10, f);
        else
            throw ("error: incorrect char of one of argument: " + str.at(i));

    return result;
}

void TTableViewer::Delete(string str) {
    Table->DelRecord(str);
    if (Table->GetRetCode() == TabNoRec)
        throw "No record";
}

void TTableViewer::Find(string str) {
    Marks* Mark = (Marks*)Table->FindRecord(str);
    if (Table->GetRetCode() == TabNoRec)
        throw "No record";
    else {
        cout << str << " ";
        for (int i = 0; i < Subjects; i++)
            cout << Mark->Values[i] << " ";
        cout << endl;
    }
}

void TTableViewer::Insert(string str) {
    Marks* Mark = new Marks();
    if (arg.size() - 2 == Subjects)
        for (int i = 0; i < Subjects; i++)
            Mark->Values[i] = StrToInt(arg.at(i + 2));
    else
        throw "incorrect arguments";
    Table->InsRecord(str, (TDatValue*)Mark);
    if (Table->GetRetCode() == TabNoMem)
        throw "Not enough memory";
}

void TTableViewer::Replace(string str) {
    Marks* Replacement = new Marks();
    if (arg.size() - 2 == Subjects)
        for (int i = 0; i < Subjects; i++)
            Replacement->Values[i] = StrToInt(arg.at(i + 2));
    else
        throw "incorrect arguments";
    Marks* Mark = (Marks*)Table->FindRecord(str);
    if (Table->GetRetCode() == TabNoRec)
        throw "No record";
    else
        for (int i = 0; i < Subjects; i++)
            Mark->Values[i] = Replacement->Values[i];
}

void TTableViewer::Save(string str) {
    ofstream strm(str);
    string Key;
    Marks* Mark;
    if (strm.is_open()) {
        Table->Reset();
        for (int i = 0; i < Table->GetDataCount(); i++)
        {
            Key = Table->GetKey();
            Mark = (Marks*)Table->GetValuePtr();
            strm << "\"" <<  Key << "\"" << " ";
            for (int f = 0; f < Subjects; f++)
                strm << Mark->Values[f] << " ";
            strm << endl;
            Table->GoNext();
        }
    }
    else
        throw "cant open the file";
    strm.close();
}

void TTableViewer::Load(string str) {
    ifstream strm(str);
    string line;
    vector<string> args;
    string Key;
    TTable* TableBuffer;

    switch (Use) {
		case Tree: TableBuffer = new TTreeTable(); break;
    case Sort: TableBuffer = new TSortTable(); break;
    case Hash: TableBuffer = new TArrayHash(); break;
    default: TableBuffer = new TScanTable(); break;
    }

    if (strm.is_open()) {
        while (!strm.eof()) {
            getline(strm, line);
            if (line != "") {
                Marks* Mark = new Marks();
                args = MessageToArgs(line);
                if (args.size() != Subjects + 1)
                    throw "incorrect file input, check file to resolve the problem";
                Key = args.at(0);
                for (int i = 0; i < Subjects; i++)
                    Mark->Values[i] = StrToInt(args.at(i + 1));
                TableBuffer->InsRecord(Key, (PTDatValue)Mark);
                if (TableBuffer->GetRetCode() != TabOK)
                    throw "something going wrong";
            }
        }
        delete Table;
        Table = TableBuffer;
        line.clear();
    }
    else
        throw "cant open the file";

    strm.close();
}

void TTableViewer::Show() {

    Marks* Mark;

    Table->Reset();

    for (int i = 0; i < Table->GetDataCount(); i++) {
        cout << Table->GetKey() << " ";
        Mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++)
            cout << Mark->Values[f] << " ";
        cout << endl;
        Table->GoNext();
    }
}

double TTableViewer::AverageMarks() {
    int avrg = 0;
    Table->Reset();
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++)
            avrg += mark->Values[f];
        Table->GoNext();
    }
    if (Table->GetDataCount() != 0)
        return (double)avrg / (double)(Subjects*Table->GetDataCount());
    else
        return avrg;
}

string TTableViewer::BestStudent() {
	  Table->Reset();
    int avrg1 = 0;
    int avrg2 = 0;
    string name("");
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++)
            avrg2 += mark->Values[f];
        if (avrg2 > avrg1) {
            name = Table->GetKey();
            avrg1 = avrg2;
        }
        avrg2 = 0;
        Table->GoNext();
    }
    return name;
}

double TTableViewer::AverageMarkOfSubject(int NumOfSub) {
    int avrg = 0;
    Table->Reset();
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        avrg += mark->Values[NumOfSub];
        Table->GoNext();
    }
    if (Table->GetDataCount() != 0)
        return (double)avrg / (double)(Table->GetDataCount());
    else
        return avrg;
}

void TTableViewer::InformationAboutStudent(string student) {
    Marks* mark = (Marks*)Table->FindRecord(student);
    if (Table->GetRetCode() == TabNoRec)
        throw "This student dont exist";
    else {
        int avrg = 0;
        cout << "Marks: ";
        for (int i = 0; i < Subjects; i++) {
            cout << mark->Values[i] << " ";
            avrg += mark->Values[i];
        }
        cout << endl;
        cout << "Average of marks: " << (double)avrg/ (double)Subjects << endl;
    }
}

void TTableViewer::HighAchievers() {
    Marks* mark;
    bool Good = true;
    Table->Reset();
    for (int i = 0; i < Table->GetDataCount(); i++) {
        Marks* mark = (Marks*)Table->GetValuePtr();
        for (int f = 0; f < Subjects; f++) {
            if (mark->Values[f] != 5)
                Good = false;
        }
        if (Good)
            cout << Table->GetKey() << endl;
        Good = true;
        Table->GoNext();
    }
}

void TTableViewer::Help() {
    cout << "commands: " << endl;
    cout << "save, save \"path\", load \"path\" - save or load table" << endl;
    cout << "<Marks> means sequence of mark, like <mark1> <mark2> ... <markN>" << endl;
    cout << "insert/replace \"key\" <Marks> - insert or replace marks" << endl;
    cout << "delete \"key\" - delete string" << endl;
    cout << "show - print table on console" << endl;
    cout << "avrg - average of marks" << endl;
    cout << "best - best student" << endl;
    cout << "highach - list of high achievers" << endl;
    cout << "studinf \"name\" - information about student" << endl;
    cout << "avrgofsub <NumberOfSubject> - average mark of one subject" << endl;
    cout << "exit, quit, q - quit the program" << endl;
    cout << "subjects: " << endl;
    cout << "0 - AlgorithmsAndStructureOfData, 1 - OptimizationMetods, 2 - OperationSystems" << endl;
    cout << "3 - DevelopingPO, 4 - DevelopingInstruments" << endl;
    cout << "Credits: " << endl;
    cout << "Table editor by Aleksandr Nazarov and Mihail Gorozhanin" << endl;
}