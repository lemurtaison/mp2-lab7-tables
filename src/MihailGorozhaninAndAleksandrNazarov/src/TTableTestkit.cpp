
#include "../include/TTableViewer.h"
#include <iostream>

int main() {
    TTableViewer Viewer;
    string command = "";
    while (command != "quit" && command != "q" && command != "exit") {
        try {
            command = "";
            getline(cin, command);
            Viewer.PushMessage(command);
        }
        catch (char* err) {
            cout << err << endl;
        }
        catch (string err) {
            cout << err << endl;
        }
    }
    return 0;
}