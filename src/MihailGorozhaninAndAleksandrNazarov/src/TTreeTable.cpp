#include "../include/TTreeTable.h"
#include "../include/TTreeNode.h"

bool TTreeTable::IsFull() const
{
	return false;
}

PTDatValue TTreeTable::FindRecord(TKey k) {
	PTTreeNode tmp = pRoot;
	ppRef = &pRoot;
	while (tmp != nullptr) {
		if (tmp->GetKey() == k) break;
		if (tmp->GetKey() < k) ppRef = &tmp->pRight;
		else ppRef = &tmp->pLeft;
		tmp = *ppRef;
	}
	if (tmp == nullptr) {
		SetRetCode(TabNoRec);
		return nullptr;
	}
	else {
		SetRetCode(TabOK);
		return tmp->GetValuePtr();
	}
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(TabNoMem);
	}
	else {
		if (FindRecord(k) != nullptr) {
			SetRetCode(TabNoRec);
		}
		else {
			*ppRef = new TTreeNode(k, pVal);
			DataCount++;
		}
	}
}

void TTreeTable::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(TabNoRec);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}
		// �������� �����
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					else if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// �������� ����� � ����� �������� (������)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// �������� ����� � ����� �������� (�����)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// �������� ����� � ����� ���������
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}

	}
}

TKey TTreeTable::GetKey(void) const
{
	return (pCurrent == nullptr) ? "" : pCurrent->GetKey();
}

PTDatValue TTreeTable::GetValuePtr(void) const
{
	return (pCurrent == nullptr) ? nullptr : pCurrent->GetValuePtr();
}

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
	if (pNode != nullptr) {
		DeleteTreeTab(pNode->GetLeft());
		DeleteTreeTab(pNode->GetRight());
		delete pNode;
	}
}

void TTreeTable::Reset(void)
{
	PTTreeNode pNode = pCurrent = pRoot;
	CurrPos = 0;
	while (pNode != nullptr) {
		St.push(pNode);
		pCurrent = pNode;
		pNode = pNode->GetLeft();
	}
	SetRetCode(TabOK);
}

bool TTreeTable::IsTabEnded(void) const
{
	return (CurrPos >= DataCount);
}

int TTreeTable::GoNext(void)
{
	CurrPos++;
	if (!IsTabEnded() && (pCurrent != nullptr)) {
		PTTreeNode pNode = pCurrent = pCurrent->GetRight();
		St.pop();
		while (pNode != nullptr) {
			St.push(pNode);
			pCurrent = pNode;
			pNode = pNode->GetLeft();
		}
		if ((pCurrent == nullptr) && !St.empty())
			pCurrent = St.top();

	}
	else SetRetCode(TabNoRec);
	return GetRetCode();
}