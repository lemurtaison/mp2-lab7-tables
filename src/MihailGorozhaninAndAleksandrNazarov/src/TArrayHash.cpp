#include "../include/TArrayHash.h"

#include <iostream>

TArrayHash::TArrayHash(int size) : THashTable() {
    TabSize = size;
    HashStep = 1;
    pRecs = new PTTabRecord[TabSize];
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = 0;
    DataCount = 0;
}

TArrayHash::~TArrayHash() {
    delete[] pRecs;
}

bool TArrayHash::IsFull() const {
    return DataCount >= TabSize;
}

TKey TArrayHash::GetKey(void) const {
    return pRecs[CurrPos]->GetKey();
}

PTDatValue TArrayHash::GetValuePtr(void) const {
    if (pRecs[CurrPos] != 0)
        return pRecs[CurrPos]->GetValuePtr();
    else
        return 0;
}

PTDatValue TArrayHash::FindRecord(TKey k) {
    CurrPos = HashFunc(k) % TabSize;
    for (int i = 0; i < TabSize; i++)
    {
        Efficiency++;
        if (pRecs[CurrPos] == 0)
            break;
        else if (pRecs[CurrPos]->GetKey() == k)
            return pRecs[CurrPos]->GetValuePtr();
        CurrPos = GetNextPos(CurrPos);
    }
    SetRetCode(TabNoRec);
    return 0;
}

void TArrayHash::InsRecord(TKey k, PTDatValue pVal) {
    CurrPos = HashFunc(k) % TabSize;
    for (int i = 0; i < TabSize; i++)
    {
        Efficiency++;
        if (pRecs[CurrPos] != 0 && pRecs[CurrPos]->GetKey() == k)
            return;
        else if (pRecs[CurrPos] == 0)
        {
            pRecs[CurrPos] = new TTabRecord(k, pVal);
            DataCount++;
            return;
        }
        CurrPos = GetNextPos(CurrPos);
    }
    SetRetCode(TabNoMem);
}

void TArrayHash::DelRecord(TKey k) {
    PTDatValue record = FindRecord(k);
    if (record != 0)
    {
        delete pRecs[CurrPos];
        pRecs[CurrPos] = 0;
        DataCount--;
    }
    else
        SetRetCode(TabNoRec);
}

void TArrayHash::Reset(void) {
    CurrPos = 0;
    GoNext();
}

bool TArrayHash::IsTabEnded(void) const {
    return CurrPos >= TabSize;
}

int TArrayHash::GoNext(void) {
    if (IsTabEnded())
        SetRetCode(TabNoRec);
    else
        while (++CurrPos < TabSize)
            if (pRecs[CurrPos] != 0)
                break;
    return GetRetCode();
}