#include "../include/TBalanceTree.h"

Height TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
	Height HeightIndex = Height::OK;
	if (pNode == nullptr) { // ������� �������
		pNode = new TBalanceNode(k, pVal);
		HeightIndex = Height::Inc;
		DataCount++;
	}
	else if (k < pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pLeft, k, pVal) == Height::Inc) {
			HeightIndex = LeftTreeBalancing(pNode);
		}
	}
	else if (k > pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pRight, k, pVal) == Height::Inc) {
			HeightIndex = RightTreeBalancing(pNode);
		}
	}
	else {
		SetRetCode(TabNoRec);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalRight:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalLeft);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalLeft:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetLeft());
		if (p1->GetBalance() == Bal::BalLeft) {
			pNode->pLeft = p1->pRight;
			p1->pRight = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetRight());
			p1->pRight = p2->pLeft;
			p2->pLeft = p1;
			pNode->pLeft = p2->pRight;
			p2->pRight = pNode;
			if (p2->GetBalance() == Bal::BalLeft) {
				pNode->SetBalance(Bal::BalRight);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalRight) {
				p1->SetBalance(Bal::BalLeft);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalLeft:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalRight);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalRight:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetRight());
		if (p1->GetBalance() == Bal::BalRight) {
			pNode->pRight = p1->pLeft;
			p1->pLeft = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetLeft());
			p1->pLeft = p2->pRight;
			p2->pRight = p1;
			pNode->pRight = p2->pLeft;
			p2->pLeft = pNode;
			if (p2->GetBalance() == Bal::BalRight) {
				pNode->SetBalance(Bal::BalLeft);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalLeft) {
				p1->SetBalance(Bal::BalRight);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}


void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(TabNoMem);
	}
	else {
		InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
	}
}

void TBalanceTree::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(TabNoRec);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}

		TKey k2 = tmp->GetKey();
		// �������� �����
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// �������� ����� � ����� �������� (������)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// �������� ����� � ����� �������� (�����)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// �������� ����� � ����� ���������
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		if (pRoot != nullptr) {
			if (k2 < pRoot->GetKey()) {
				LeftTreeBalancing((PTBalanceNode&)pRoot);
			}
			else  if (k2 > pRoot->GetKey()) {
				RightTreeBalancing((PTBalanceNode&)pRoot);
			}
		}
	}
}