#include "../include/TTreeNode.h"

PTTreeNode TTreeNode::GetLeft(void) const
{
	return pLeft;
}


PTTreeNode TTreeNode::GetRight(void) const
{
	return pRight;
}

TDatValue * TTreeNode::GetCopy()
{
	TTreeNode *tmp = new TTreeNode(Key, pValue, nullptr, nullptr);
	return tmp;
}