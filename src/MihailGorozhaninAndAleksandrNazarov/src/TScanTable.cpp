
#include "..\include\TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k) {
    int i;
    SetRetCode(TabOK);
    for (i = 0; i < DataCount && !(pRecs[i]->Key == k); i++);
    Efficiency = i + 1;
    if (i < DataCount) {
        CurrPos = i;
        return pRecs[i]->pValue;
    }
    SetRetCode(TabNoRec);
    return NULL;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal) {
    if (IsFull()) {
        SetRetCode(TabNoMem);
        throw "Not enough memory";
    }
    else {
        pRecs[DataCount++] = new TTabRecord(k, pVal);
    }
}

void TScanTable::DelRecord(TKey k) {
    for (int i = 0; i < DataCount; i++) {
        if (pRecs[i]->GetKey() == k) {
            for (int j = i; pRecs[j + 1] != 0 && j < TabSize; j++)
                pRecs[j] = pRecs[j + 1];
            DataCount--;
            return;
        }
    }
}